<!DOCTYPE html>
<html>
<head>
	<title>Cotizaciones a registrar</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>
</head>
	
<body>
	 <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
                <?php
                $sel = $con->prepare("SELECT *from cotizacion guardar");
                $sel->execute();
                $res = $sel->get_result();
                ?>
                <div class="h2">
                    Mostrar cotizaciones que se han guardado
                </div>
                <div class="h3">
                    Para guardar las cotizaciones, primero se debe a&ntilde;adir a la base de datos
                </div>
                <div class="h4">
                    1.- Selecciona la cotizacion que desea guardar de la refacci&oacute;n
                </div>
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <th>ID COTIZACION A GUARDAR</th>
                    <th>NOMBRE COTIZACION A GUARDAR</th>
                    </thead>
                    <tfoot>
                    <th>ID COTIZACION A GUARDAR</th>
                    <th>NOMBRE COTIZACION A GUARDAR</th>
                    </tfoot>
                    <tbody>
                        <?php while ($f = $res->fetch_assoc()) { ?>
                            <tr>
                                <td>
                                    <?php echo $f['marca_id'] ?>
                                </td>
                                <td>
                                    <a href="cotizacion_guardar.php?marca_id=<?php echo $f['marca_id']?>&marca_nombre=<?php echo $f['marca_nombre'] ?>"><?php echo $f['marca_nombre'] ?></a>
                                </td>
                            </tr>
                            <?php
                        }
                        $sel->close();
                        $con->close();
                        ?>
                    <tbody>
                </table>
            </div>
        </div>
        <?php include'inc/incluye_datatable_pie.php' ?>
    </body>
</html>
</html>