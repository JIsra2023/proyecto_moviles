<!DOCTYPE html>
<html>
<head>
	<title>Agregar Nueva Sucursal</title>
	<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--código que incluye Bootstrap-->
        <?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>
</head>
<body>
	<!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
            <form role="form" id="login-form" method="post" class="form-signin" action="sucursal_guardar.php">
            	 <?php
                $sel = $con->prepare("SELECT *from sucursal_prov");
                $sel->execute();
                $res = $sel->get_result();
                ?>

                <div class="h1">Agregar Nueva Sucursal</div>
                <div class="h2">Informacion de la nueva sucursal</div>

<div class="form-group">
            <div class="dropdown">
                
               <label>Seleccione un proveedor</label>
               <br>
               <button class="btn btn-primary dropdown-toggle" class="caret" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true" required>
                <font color="black"> 
               <select name="proveedor" required>
                    <option value="0" class="form-control" id="proveedor"> SELECCIONE</option>
                    <?php 
                    $sel = $con->prepare("SELECT *from proveedor");
                    $sel->execute();
                    $res = $sel->get_result();
                    
                    while ($f = mysqli_fetch_array($res)) { 
                         echo '<option value="'.$f['proveedor_id'].'">'.$f['proveedor_nombre'].'</option>';
                        }
                        $sel->close();
                        $con->close();
                    ?>
                      </select>
                      </font>   
               </button>
                </div>
            </div>

                <div class="form-group">
                        <label>Ingresar Nombre de la Sucursal</label>
                         <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese nombre de la sucursal" style="text-transform:uppercase;" required>
                </div>
                <div class="form-group">
                        <label>Ingresar Direcci&oacute;n</label>
                         <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Ingresa dirección" style="text-transform:uppercase;">
                </div>

                <div class="form-group">
                    <label>Tel&eacute;fono 1</label>
                        <input type="text" name="telefono1" class="form-control" id="telefono1" placeholder="Ingresa primer telefono" style="text-transform:uppercase;">

                 </div>

                 <div class="form-group">
                    <label>Teléfono 2</label>
                        <input type="text" name="telefono2" class="form-control" id="telefono2" placeholder="Ingresa segundo telefono" style="text-transform:uppercase;">
                 </div>

                <div class="form-group">
                    <label for="correo">Correo Electr&oacute;nico</label>
                        <input type="email" class="form-control" id="correo" name="correo"
                               placeholder="Ingresa correo electr&oacute;nico" style="text-transform:uppercase;">
                 </div>
                 <br>
                 <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
            </form>
            </div>
        </div>
</body>
</html>
