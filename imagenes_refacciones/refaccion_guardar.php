<?php

include 'inc/conexion.php';
$refaccion_id = "";
$marca_id_post = $_POST['marca_id'];
$nombre_refaccion_post = strtoupper($_POST['nombre_de_refaccion']);
$descripcion_refaccion_post = strtoupper($_POST['descripcion_de_refaccion']);
$refaccion_imagen="sin imagen";
try {
    $sel = $con->prepare("SELECT refaccion_id,marca_id,refaccion_nombre FROM refaccion where marca_id=? AND refaccion_nombre=?");
$refaccion_imagen = "imagenes_refacciones/default.png";

$sel = $con->prepare("SELECT refaccion_id,marca_id,refaccion_nombre FROM refaccion where marca_id=? AND refaccion_nombre=?");
$sel->bind_param('is', $marca_id_post, $nombre_refaccion_post);
$sel->execute();
$res = $sel->get_result();
} else {
    $ins = $con->prepare("INSERT INTO refaccion VALUES(?,?,?,?,?)");
    $ins->bind_param("iisss", $id, $marca_id_post, $nombre_refaccion_post, $descripcion_refaccion_post, $refaccion_imagen);
    //insertar aquí los alert
    if ($ins->execute()) {
        
         header("Location: alerta.php?tipo=exito&operacion=Refacción guardada. Ahora debes agregarle un precio de proveedor Registrado (si no lo haces puedes provocar pérdida de información)&destino=refacciones_seleccionar_marca.php");
//SUBIR LA IMAGEN
        $ultimo_id = "noWhile";
        $extension = '';
        $ruta = 'imagenes_refacciones';
        $archivo = $_FILES['foto']['tmp_name']; //foto es el name del input en el formulario
        $nombrearchivo = $_FILES['foto']['name'];
        $info = pathinfo($nombrearchivo);
        if ($archivo != '') {
            $extension = $info['extension'];
            if ($extension == "png" || $extension == "PNG" || $extension == "JPG" || $extension == "jpg") {
                //CONSULTAMOS EL ID DEL ULTIMO REGISTRO SUBIDO
                $sel = $con->prepare("SELECT MAX(refaccion_id)as ultima_refaccion FROM refaccion;");
                $sel->execute();
                $res = $sel->get_result();
                while ($f = $res->fetch_assoc())
                    $ultimo_id = $f['ultima_refaccion'];

                //FIN //CONSULTAMOS EL ID DEL ULTIMO REGISTRO SUBIDO
                //SUBIMOS LA IMAGEN CAMBIANDO SU NOMBRE POR EL DEL ULTIMO ID
                move_uploaded_file($archivo, 'imagenes_refacciones/' . $ultimo_id . '.' . $extension);
                $ruta = $ruta . "/" . $ultimo_id . '.' . $extension;
                //FIN SUBIMOS LA IMAGEN CAMBIANDO SU NOMBRE POR EL DEL ULTIMO ID
                //ACTUALIZAMOS EL CAMPO QUE CONTIENE EL NOMBRE DE LA IMAGEN EN LA BD
                $sel = $con->prepare("UPDATE refaccion SET refaccion_imagen='" . $ruta . "' WHERE refaccion_id=?;");
                $sel->bind_param('i', $ultimo_id);
                $sel->execute();
                //FIN ACTUALIZAMOS EL CAMPO QUE CONTIENE EL NOMBRE DE LA IMAGEN EN LA BD
                header("Location: alerta.php?tipo=exito&operacion=Refaccion guardada, imagen almacenada&destino=refacciones_seleccionar_marca.php?");
            } else {
                header("Location: alerta.php?tipo=fracaso&operacion=Formato de imagen no valido, utiliza jpg o png &destino=refacciones_seleccionar_marca.php");
            }
        } else {

            header("Location: alerta.php?tipo=exito&operacion=Refaccion guardada, no se selecciono imagen&destino=refacciones_seleccionar_marca.php?");
        }
    } else {
        echo "Error al insertar Refaccion";
        header("Location: alerta.php?tipo=fracaso&operacion=No se pudo registrar refaccion&destino=refacciones_seleccionar_marca.php");
    }
    $ins->close();
    $con->close();
} 
catch (Exception $e) {
   $e->getMessage(header("Location: alerta.php?tipo=fracaso&operacion=Refacción no guardada, probablemente ya se encuentre registrada&destino=refacciones_agregar.php")); 
   
}
?>

